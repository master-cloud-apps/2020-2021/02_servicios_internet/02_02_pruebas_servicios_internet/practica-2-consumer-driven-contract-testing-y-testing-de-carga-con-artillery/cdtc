package es.codeurjc.mastercloudapps.planner.clients.cdct;

import es.codeurjc.mastercloudapps.planner.models.LandscapeResponse;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties.StubsMode;
import org.springframework.web.client.RestTemplate;

import static es.codeurjc.mastercloudapps.planner.clients.TopoClient.TOPO_HOST;
import static es.codeurjc.mastercloudapps.planner.clients.TopoClient.TOPO_PORT;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@AutoConfigureStubRunner(
        ids={"apeyo2020.cdct:toposervice:+:stubs:"+TOPO_PORT},
        stubsMode = StubsMode.LOCAL
)
public class TopoServiceContractVerifierTest {

    private RestTemplate restTemplate;

    @BeforeEach
    void setUp(){
        this.restTemplate = new RestTemplate();
    }

    @ParameterizedTest
    @CsvSource({"Madrid,Flat", "Barcelona,Flat"})
    public void verify_city_topo_details(String city, String landscape){
        String url = "http://"+TOPO_HOST+":"+TOPO_PORT+"/api/topographicdetails/" + city;

        LandscapeResponse response = restTemplate.getForObject(url, LandscapeResponse.class);

        assertEquals(city, response.getId());
        assertEquals(landscape, response.getLandscape());
    }
}
